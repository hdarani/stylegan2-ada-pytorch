#!/bin/bash
#SBATCH --job-name=style_transfer
#SBATCH --mail-type=END,FAIL            # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=hossein.darani@sanctuary.ai   # Where to send mail
#SBATCH --output=/share/storage/home/hossein_darani/stylegan2-ada-pytorch/log/bb_synth.txt
#
#SBATCH --ntasks=1
#SBATCH -c 1
#SBATCH --gres=gpu:1
# #SBATCH --time=20:00:00
# #SBATCH --begin=now+7hour
#SBATCH --array=1-1 #24 jobs for cut and fast cut - 8 jobs for sincut

# What to change? --output and --model sincut and array size

if [ $SLURM_ARRAY_TASK_ID == 1 ]; then
        chmod 777 /share/storage/home/hossein_darani/stylegan2-ada-pytorch/log
        chmod 777 /share/storage/home/hossein_darani/stylegan2-ada-pytorch/train_logs
fi
# TODO It should not be pushed in git - must be private
export WANDB_API_KEY=6a2823d4cebeec17e844071b9e4b2edba52ae8fa

srun singularity exec --nv sg2_docker.sif \
                /env/bin/python /share/storage/home/hossein_darani/stylegan2-ada-pytorch/train.py \
                --outdir train_logs/training-runs \
                --data dataset/bb_test.zip \
                --gpus 1 \
